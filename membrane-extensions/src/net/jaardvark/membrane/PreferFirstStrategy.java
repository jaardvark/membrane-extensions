package net.jaardvark.membrane;

import java.util.List;

import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;

import com.predic8.membrane.annot.MCElement;
import com.predic8.membrane.core.config.AbstractXmlElement;
import com.predic8.membrane.core.exchange.AbstractExchange;
import com.predic8.membrane.core.interceptor.balancer.DispatchingStrategy;
import com.predic8.membrane.core.interceptor.balancer.EmptyNodeListException;
import com.predic8.membrane.core.interceptor.balancer.LoadBalancingInterceptor;
import com.predic8.membrane.core.interceptor.balancer.Node;

/**
 * Strategy which always prefers the first configured endpoint.
 * In this way we use the same endpoint normally, but failover to another configured endpoint on error.
 * This is useful in georedundant scenarios where the failover endpoint is far, and hence slower, so you
 * prefer your close endpoint normally, and use the far one only on error.
 * @author Richard Unger
 */
@MCElement(name="preferFirstStrategy")
public class PreferFirstStrategy extends AbstractXmlElement implements DispatchingStrategy {

	@Override
	public Node dispatch(LoadBalancingInterceptor interceptor) throws EmptyNodeListException {
		List<Node> endpoints = interceptor.getEndpoints(); //this calls synchronizes access internally.
        if (endpoints.isEmpty()) {
            throw new EmptyNodeListException();
        }
        return endpoints.get(0);
	}

	@Override
	public void done(AbstractExchange exc) {
		// nix
	}

	@Override
	public void write(XMLStreamWriter out) throws XMLStreamException {
		out.writeStartElement("preferFirstStrategy");
		out.writeEndElement();
	}
	
}
